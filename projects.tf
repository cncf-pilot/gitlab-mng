resource "gitlab_project" "README-cncf" {
  name                   = "README-cncf"
  description            = "Overview of GitOps-Pilot Group"
  default_branch         = "master"
  namespace_id           = data.gitlab_group.cncf-pilot.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}
/*
resource "gitlab_project" "cncf-api-guide" {
  name                   = "cncf-api-guide"
  description            = "AutoDevOps with Dockerfile and default template"
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}
*/
/*
resource "gitlab_project" "my-spring-app" {
  name                   = "My Spring App2"
  description            = "AutoDevOps with Dockerfile and default template"
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "templates" {
  name             = "templates"
  default_branch   = "master"
  namespace_id     = gitlab_group.infra.id
  visibility_level = "public"
}
resource "gitlab_project" "aws" {
  name                   = "aws"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "gcp" {
  name                   = "gcp"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "azure" {
  name                   = "azure"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "cluster-management" {
  name = "cluster-management"
  description = "GitLab Managed Apps code based installation project"
  default_branch = "master"
  namespace_id = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level = "public"
}*/
