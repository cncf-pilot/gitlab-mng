// gitlab의 group name Setting.
data "gitlab_group" "cncf-pilot" {
  full_path = "cncf-pilot"
}

// gitlab의 subgroup name Setting.
resource "gitlab_group" "apps" {
  name                = "apps"
  description         = "CNCF Applications"
  path                = "apps"
  parent_id           = data.gitlab_group.cncf-pilot.id
  visibility_level    = "public"
  auto_devops_enabled = "true"
}

resource "gitlab_group" "infra" {
  name                = "infra"
  description         = "Infrastructure Projects"
  path                = "infra"
  parent_id           = data.gitlab_group.cncf-pilot.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}
