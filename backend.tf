terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "cncf2jds"

    workspaces {
      name = "gitlab-mng"
    }
  }
}